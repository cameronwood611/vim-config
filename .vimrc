" plugins
call plug#begin()
Plug 'https://github.com/w0rp/ale.git'	"a linter
Plug 'https://github.com/vim-airline/vim-airline' "Vim buffer thing
"Plug 'https://github.com/python/black' "python code formatter
Plug 'https://github.com/scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
call plug#end()

" visual settings
syntax enable
set ruler
set wrap
set linebreak
set showmatch

" functional settings
set autoindent
set smartindent
set smarttab
set expandtab
set tabstop=4
set shiftwidth=4
set encoding=utf8
set termencoding=utf8
set autochdir
set backspace=indent,eol,start
" set spell
set ttyfast 						"redraw faster
set undofile
set undodir=~/.vim/undodir
set mouse=a

" search settings
set ignorecase
set smartcase
set hlsearch
set incsearch


" information settings
set number
set relativenumber
set laststatus=2
set wildmenu
set wildmode=longest,full,list
set title
set history=1000

let g:airline_powerline_fonts = 0
let g:airline_symbols_ascii = 1

let NERDTreeShowHidden=1
map <F12> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
